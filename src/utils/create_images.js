const fs = require('fs');
const svg2img = require('svg2img');
const xlsxFile = require('read-excel-file/node')
/** Le o arquivo do excel e chama a função de criar imagem */
xlsxFile('src/assets/COR.xlsx').then((rows) => {
    for (let index = 1; index <= (rows.length - 1); index++) {
        create_image(rows[index][0], rows[index][1])
    }

})

/**Cria a imagem usando svg 
 * @param {string} color -cor da imagem
 * @param {string} name -nome da imagem sem extensão 
*/
create_image = (color, name) => {
    let svgString = [
        '<svg width="24" height="24">',
        `<circle r="60" stroke-width="2" fill="${color}" />`,
        '</svg>',
    ].join('');
    //converte a imagem para png  
    svg2img(svgString, function (error, buffer) {
        //returns a Buffer
        fs.writeFileSync(`src/assets/${name}.png`, buffer);
    });

}

