/**Deleta todas as mensagens do canal
 * @param {Client} el - discord_client
 */
delete_messages = async (el) => {
	/** Recupera o canal do bot pelo id na variavel de ambiente */
	const channel = await el.channels.fetch(process.env.BOT_CHANNEL);
	/**Recupera todas as mensagens do canal */
	const fetched = await channel.messages.fetch({ limit: 99 });
	/**Deleta tudo o que achou de mensagens */
	channel.bulkDelete(fetched);
};

module.exports.delete_messages;
