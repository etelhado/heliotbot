const xlsxFile = require('read-excel-file/node');
const Discord = require('discord.js');
/**
 * Enviar tabela de cores para o discord
 * @param {Client} el - Discord Client
 */
send_colors = async (el) => {
	/**recupera o canal do bot da variavel de ambiente */
	const channel = await el.channels.fetch(process.env.BOT_CHANNEL);
	/**le o arquivo excel da pasta assets */
	const rows = await xlsxFile('src/assets/COR.xlsx');
	try {
		/**faz um parse linha a linha pegando as cores das colunas */
		for (let index = 1; index <= rows.length - 1; index++) {
			/**Faz o upload da imagem da cor na pasta assets */
			let attachment = new Discord.MessageAttachment(
				`src/assets/${rows[index][1]}.png`,
				`${rows[index][1]}.png`,
			);
			/**cria o embed com os dados das cores */
			let embed = new Discord.MessageEmbed()
				.attachFiles(attachment)
				.setColor(`${rows[index][0]}`)
				.setAuthor(`${rows[index][1]}`, `attachment://${rows[index][1]}.png`)
				.setDescription(`comprar |${rows[index][1]}`)

			/**envia a mensagem */
			let message = await channel.send(embed);
			/**ativa a reação na mensagem */
			message.react("🎨");
		}
	} catch (error) {
		console.log('ocorreu um erro', error);
	}
};
