require('dotenv').config();
require('./utils/delete_messages')
require('./utils/send_colors')

const Discord = require('discord.js');
const elliot = new Discord.Client();

/**
 * Inicio do Elliot.
 * Deleta todas as mensagems no canal 
 * Gera as cores com a reação
 */
elliot.on("ready", () => {
    console.log("Successfully logged into client.");
    delete_messages(elliot);
    send_colors(elliot);
});

/**
 * Função para ler os comandos 
 * não Iimplementado
 */
elliot.on('message', (message) => {
    if (message.author.bot) return;
    if (!message.content.startsWith(process.env.BOT_PREFIX)) return;
    if (message.content.split(process.env.BOT_PREFIX)[1] === 'Cargo' ) {
        console.log('Cargo')
        let role = message.guild.roles.cache.find(role => role.id == 754103815875199036);
        console.log(role)

        let guildMember = message.member
        /**Aplica a role caso a mesma exista */
        role ? guildMember.roles.add(role) : null
    }
});

/**
 * Escutar as reações no canal expecificado
 * @param {MessageReaction} reaction - a reação do usuario
 * @param {PartialUser} user - o usuario da reação
 */

elliot.on('messageReactionAdd', (reaction, user) => {
    let message = reaction.message, emoji = reaction.emoji;
    /**Compara o emoji da reaction*/
    if (emoji.name == '🎨' && user.id != process.env.BOT_ID) {
        /**Separa a mensagem em 2 partes pelo delimitador "|" */
        let role_name = reaction.message.embeds[0].description.split('|')[1]
        /**Recupera a role pelo nome */
        let role = message.guild.roles.cache.find(role => role.name ==  '🎨'+ role_name);
        /**Recupera o membro do canal  */
        let guildMember = message.guild.members.cache.find(member => member.id == user.id)
        guildMember.roles.cache.forEach(rle => {
            if ( rle.name.includes('🎨') ) {
                guildMember.roles.remove(rle)
            }
        })
        /**Aplica a role caso a mesma exista */
        role ? guildMember.roles.add(role) : null
        message.reactions.removeAll();
        message.react("🎨");
    }
});

elliot.login(process.env.BOT_KEY);
